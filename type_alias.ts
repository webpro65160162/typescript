type CarYear = Number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel
}

const carYear: CarYear = 2001;
const cartype: CarType = "Nissan";
const carModel: CarModel = "XX"

const car11: Car = {
    year: carYear,
    type: cartype,
    model: carModel
}
console.log(car11)